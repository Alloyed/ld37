LUDUM DARE 37


Hi, this is the source code for ADC, which is a bot to play an IF game from
within discord. It uses the luvit and discordia libraries, which you should
google to find install instructions. We also use redis for game state, so
you should get that too.

There are a bunch of hardcoded numbers I added just to get this out the door.
The obvious ones the list of discord uids at the top of `bot.lua`, but you
there might be others if you try to run this for yourself, sorry~

Once you've got the deps installed, redis is running and all that, you can
start the game using:

	ADCBOT_TOKEN="your token" ./luvit main.lua

