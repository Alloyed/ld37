log, dbg = p, p

local ONE_ROOM_ID  = "257230731586174984"
local TEST_ROOM_ID = "257232986645200896"
local RPLEBS_ID    = "257230838041804800"
local YPLEBS_ID    = "257600552228225025"
local BPLEBS_ID    = "257600524159942666"
local WINNERS_ID   = "257227306903404545"
local ADMINS_ID    = "257235663873638400"
local ADC_ID       = "256984705050738699"

_G.commands = {}
_G.synonyms = {}

local function is_admin(member)
	for role in member.roles do
		if role.id == ADMINS_ID then
			return true
		end
	end
	return false
end

function get_guild()
	for g in client.guilds do
		return g
	end
end

function commands._reset(reply, author, id)
	local member = author:getMembership(get_guild())
	if not is_admin(member) then return end

	if id == "" then
		reply("you forgot the id.", 'private')
		return
	end
	local found = false
	for channel in client.channels do
		if id == "all" or channel.id == id then
			found = true
			if channel.bulkDelete then
				reply("bulk deleting: "..channel.name, 'private')
				channel:bulkDelete(99)
				reply("complete!", 'private')
			end
		end
	end
	if not found then
		reply("id not found.", 'private')
	end
end

local function get_roles(guild)
	local rplebs, bplebs, yplebs, winners
	for role in guild.roles do
		if role.id == RPLEBS_ID then
			rplebs = role
		elseif role.id == BPLEBS_ID then
			bplebs = role
		elseif role.id == YPLEBS_ID then
			yplebs = role
		elseif role.id == WINNERS_ID then
			winners = role
		end
	end
	return rplebs, bplebs, yplebs, winners
end

local function is_pleb(member)
	if not member.user then
		member = member:getMembership(get_guild())
	end
	for role in member.roles do
		if role.id == RPLEBS_ID then
			return true
		elseif role.id == BPLEBS_ID then
			return true
		elseif role.id == YPLEBS_ID then
			return true
		elseif role.id == ADMINS_ID then
			return true
		end
	end
	return false
end
function is_red(user)
	local guild = get_guild() local member = user:getMembership(guild)
	for role in member.roles do
		if role.id == RPLEBS_ID then
			return true
		elseif role.id == ADMINS_ID then
			return true
		end
	end
	return false
end
function is_blue(user)
	local guild = get_guild() local member = user:getMembership(guild)
	for role in member.roles do
		if role.id == BPLEBS_ID then
			return true
		elseif role.id == ADMINS_ID then
			return true
		end
	end
	return false
end
function is_yellow(user)
	local guild = get_guild() local member = user:getMembership(guild)
	for role in member.roles do
		if role.id == YPLEBS_ID then
			return true
		elseif role.id == ADMINS_ID then
			return true
		end
	end
	return false
end

function commands._win(reply)
	local guild = get_guild()
	local rplebs, bplebs, yplebs, winners = get_roles(guild)

	local channel, winners_room
	for c in client.channels do
		if c.name == "one_room" then
			channel = c
		elseif c.name == "cool_people_room" then
			winners_room = c
		end
	end
	reply("Where did you find that.")
	timer.sleep(2500)
	client:setGameName(nil)
	timer.sleep(2500)
	channel:sendMessage("ADCBot falls to the floor, inert and unresponsive.")
	timer.sleep(1000)
	channel:sendMessage("You kick him aside, and wind begins to blow the door open.")
	timer.sleep(2000)

	channel:sendMessage([[
@here, Now you can see the other side. The blinding light of the **Cool People Room** is shining through.]])
	timer.sleep(2000)
	channel:sendMessage([[Walk on in, _Winners_, you've earned it.]])
	timer.sleep(100)
	for member in guild.members do
		if is_pleb(member) then
			timer.sleep(10)
			assert(rplebs)
			assert(bplebs)
			assert(yplebs)
			member:removeRoles(rplebs, bplebs, yplebs)
			member:addRoles(winners)
		end
	end
	timer.sleep(100)
	channel:sendMessage(winners_room.mentionString)

	timer.sleep(15000)
	channel:delete()
	redis('flushdb')
	redis('sadd', 'room', unpack(_G.initial_room))
	redis('set', 'game_name', 'Escape the (chat) room!')
	client:setGameName(redis('get', 'game_name'))
	channel = guild:createTextChannel("one_room")
	channel.topic = "Escape the (chat) room. Say hi!"
	channel:sendMessage("Welcome to the game! Say hi when you're ready to start.")
	local perms = channel:getPermissionOverwriteFor(winners)
	perms:denyAllPermissions()
end

local function strip(s)
	return s:gsub("^%s+", ""):gsub("%s+$", "")
end

require 'game'
local function parse_command(msg)
	local cmd = msg.content
	if cmd:match("cloudless darkness") then
		return "_win", {msg.author}
	elseif cmd:match("cloudless") then
		return "_what", {msg.author}
	elseif cmd:match("darkness") then
		return "_what", {msg.author}
	end

	local user_obj = "user:"..msg.author.id
	local ret = redis('hget', user_obj, 'seen')
	if ret ~= 'true' then
		redis('hset', user_obj, 'seen', true)
		return 'intro', {}
	end

	-- FIXME, you can type bare commands in DMs or if you mention the bot
	if not cmd:match("^!") then
		return
	else
		cmd = cmd:gsub("^!", "")
	end

	cmd = string.lower(cmd)
	log('!', cmd)

	for pre, post in pairs(synonyms) do
		cmd = cmd:gsub(pre, post)
	end

	if cmd:match("^look") then
		local item = cmd:match("^look(.*)$")
		item = strip(item)
		return "look", {item}
	end

	if cmd:match("^read") then
		local item = cmd:match("^read(.*)$")
		item = strip(item)
		return "read", {item}
	end

	if cmd:match("^move") then
		local item = cmd:match("^move(.*)$")
		item = strip(item)
		return "move", {item}
	end

	if cmd:match("^open") then
		local item = cmd:match("^open(.*)$")
		item = strip(item)
		return "open", {item}
	end

	if cmd:match("^attack") then
		local item = cmd:match("^attack(.*)$")
		item = strip(item)
		return "attack", {item, msg.author}
	end

	if cmd:match("^talk") then
		local item = cmd:match("^talk(.*)$")
		item = strip(item)
		return "talk", {item}
	end

	if cmd:match("^give") then
		local item, recipient = cmd:match("^give(.*)to(.*)$")
		if not item or not recipient then
			return "iunno", {}
		end
		item = strip(item)
		recipient = strip(recipient)
		return "give", {item, recipient, msg.author}
	end
	
	if cmd:match("^use") then
		local item, recipient = cmd:match("^use(.*)on(.*)$")
		if not item or not recipient then
			return "no_use", {item, recipient}
		end
		item = strip(item)
		recipient = strip(recipient)
		return "use", {item, recipient}
	end

	if cmd:match("^help") then
		return "help", {}
	end

	if cmd:match("^eat") then
		return "eat", {}
	end

	if cmd:match("^intro") then
		return "intro", {}
	end

	if cmd:match("^_reset") then
		local id = cmd:match("^_reset(.*)$")
		id = strip(id)
		return "_reset", {msg.author, id}
	end


	return "iunno", {}
end

local bot = {}
function bot.messageCreate(msg)
	-- don't reply to yourself
	if msg.author == client.user then return end
	if msg.channel.isPrivate then
		return
		-- FIXME: only respond to plebs
	elseif msg.channel.name ~= "one_room" then
		return
	end
	local function reply(m, private_only)
		if msg.channel.isPrivate then
			--return msg.channel:sendMessage(m)
		else
			return msg.channel:sendMessage(msg.author.mentionString..", "..m)
		end
	end
	local cmd, data = parse_command(msg)
	if cmd then
		local strs = {}
		for _, o in ipairs(data) do
			if type(o) == 'string' then
				table.insert(strs, string.format('%q', o))
			else
				table.insert(strs, tostring(o))
			end
		end
		local data_str = "{"..table.concat(strs, ', ') .."}"
		log(cmd, data_str)
		commands[cmd](reply, unpack(data))
	end
end

function bot.memberJoin(user)
	local guild = get_guild()
	local rplebs, bplebs, yplebs, winners = get_roles(guild)
	local plebs = {rplebs, bplebs, yplebs}

	local member = user:getMembership(guild)
	local set = {}
	for role in member.roles do
		set[role.id] = true
	end
	if not set[winners.id] then
		for _, pleb in ipairs(plebs) do
			if set[pleb.id] then
				return
			end
		end
		local pleb_n = redis('incr', 'user_count')
		pleb_n = ((tonumber(pleb_n)-1) % 3) + 1
		member:addRoles(plebs[pleb_n])
	end
end

function bot.loop()
	local function reply(text)
		local channel
		for c in client.channels do
			if c.name == "one_room" then
				channel = c
				break
			end
		end

		channel:sendMessage(text)
	end
	while true do
		timer.sleep(20000)
		client:setGameName(redis('get', 'game_name'))
		if redis('get', 'angry_pumpkin') == 'true' then
			commands["talk"](reply, "pumpkin")
		end
	end
end

return function(client)
	client:on('messageCreate', bot.messageCreate)

	client:on('memberJoin', bot.memberJoin)

	client:on('ready', function()
		client:setStatusOnline()
		p("connected", client.user.username)
		_G.redis = redis_client { host = "localhost", port = 6379 }
		p("redis client started")
		client:setGameName(redis('get', 'game_name'))
		return bot.loop()
	end)

end
