-- gameplay related stuff only
--


local items = {}
local item_aliases = {}

_G.initial_room = {
	"room", "self", "adcbot",
	"door", "pumpkin",
	"counter", "fries", "spaghetti", "salad",
	"bookshelf", "blue_book"
}

-- {{{ items
items.door = {}
items.door.name = "A locked metal `door`"
items.door.article = "the exit"
function items.door:description()
	local state = redis('get', 'door_state')
	if state == 'unlocked' then
		local guild = get_guild()
		for e in guild.emojis do
			emoji = e
			break
		end
		return emoji.string..": \"You won't be able to get through here, not while I'm in the way!\""
	end
	local msg = [[
It's a large, metal door, with the words **Cool People Only** on it.
You see a lock, but no key. Where could it be? Maybe try to `!look around`?]]
	return string.format(msg)
end
item_aliases["lock"] = "door"
item_aliases["exit"] = "door"

function items.door:use(item_name, item)
	if item_name ~= 'key' then
		return "You need a key to unlock the door, silly."
	end
	local emoji
	local guild = get_guild()
	for e in guild.emojis do
		emoji = e
		break
	end
	return function(reply)
		reply("The key slides right in, and as you turn it you hear a loud **click**.")
		redis('srem', 'room', 'key')
		redis('set', 'door_state', 'unlocked')
		timer.sleep(2000)
		reply(emoji.string..", your handsome narrator: \"O-oh, you're almost done, huh?\"")
		timer.sleep(2000)
		reply("...ᴵ ʰᵃᵈ ⁴⁸ ʰᵒᵘʳˢ ᵗᵒ ᵐᵃᵏᵉ ᵗʰᶦˢ...")
		timer.sleep(2000)
		reply(emoji.string..": \"Oh, I know!\"")
		timer.sleep(1000)
		reply("Another **click**, and the pumpkin's eyes glow red!")
		redis("set", "angry_pumpkin", "true")
		redis("set", "pumpkin_damage", "0")
		redis("set", "pumpkin_distance", "0")
		--redis("set", "game_name", "Murder Simulator 2016")
		redis('sadd', 'room', 'yellow_book', 'red_book')
		--client:setGameName("Murder Simulator 2016")
		timer.sleep(500)
		reply(emoji.string..": \"Have fun, buddy~\"")
		timer.sleep(1000)
		reply(items.pumpkin:speech())
	end
end

items.pumpkin = {}
local orders = {'fries', 'salad', 'spaghetti' }
items.pumpkin.name = "A sentient `pumpkin`"
items.pumpkin.article = "the hungry :jack_o_lantern:"
items.pumpkin.plural_article = false

function items.pumpkin.description()
	if redis('get', 'angry_pumpkin') == 'true' then
		return "That :jack_o_lantern: is coming right for you! Better do something, huh?"
	end
	local i = redis('get', 'pumpkin_state') or "0"
	i = tonumber(i)+1
	if orders[i] then
		return [[
That :jack_o_lantern: is holding a knife and fork! He looks very hungry. Maybe you could `!talk` to him?]]
	end

	return [[The hungry pumpkin is not so hungry anymore.]]
end

function items.pumpkin:speech()
	if redis('get', 'angry_pumpkin') == 'true' then
		local dmg = tonumber(redis('get', 'pumpkin_damage'))
		if dmg == 0 then
			return "The :jack_o_lantern: growls in your direction: **\"GIVE ME THE HUMAN FLESH!\"**"
		elseif dmg == 1 then
			return "The :jack_o_lantern: looks unsteady: **\"GIVE ME THE HUMAN FLESH!\"**"
		elseif dmg == 2 then
			return "The :jack_o_lantern: has fallen to the ground, but it's still crawling toward you: **\"HU...MAN FLESH\"**"
		end
	end

	local i = redis('get', 'pumpkin_state') or "0"
	i = tonumber(i)+1
	if not orders[i] then
		return ":jack_o_lantern:, mollified: \"You know, that wasn't too bad.\""
	end
	local msg = [[The :jack_o_lantern: bellows at you: **"I'M VERY HUNGRY! GIVE ME %s!"**]]
	msg = string.format(msg, items[orders[i]].punkin)
	return msg
end

function items.pumpkin:recieve(item_name, item)
	local i = redis('get', 'pumpkin_state') or "0"
	i = tonumber(i)+1
	if redis('get', 'angry_pumpkin') == 'true' and item_name == 'self' then
		return function(reply, user)
			reply("You give yourself to the pumpkin so that others might live.")
			timer.sleep(5000)
			get_guild():kickUser(user)
			reply(user.username .. " you have been kicked.")
		end
	elseif item_name == orders[i] then
		return function(reply)
			redis('incr', 'pumpkin_state')
			redis('srem', 'room', item_name)
			if i == 3 then -- spaghetti
				reply("You hear a faint clink as you pick the spaghetti up.")
				redis("sadd", "room", "key")
			end
			reply(("The pumpkin swipes up %s and devours it."):format(item.article))
			timer.sleep(1000)
			reply(":jack_o_lantern:: **\"OM NOM NOM NOM\"**")
			timer.sleep(1000)
			reply(self:speech())
		end
	else
		return ":jack_o_lantern:: **\"NO, I DON'T WANT THAT\"**"
	end
end

function items.pumpkin:attack(reply, user)
	local ret = redis('sismember', 'did_damage', user.id)

	if ret == 1 then
		reply("You swing, but the pumpkin is already smashed in from your position.")
		return
	end
	redis('incr', 'pumpkin_damage')
	redis('sadd', 'did_damage', user.id)
	if tonumber(redis('get', 'pumpkin_damage')) > 2 then
		redis("del", "angry_pumpkin", "pumpkin_damage", "pumpkin_distance")
		redis("srem", "room", "pumpkin")
		redis("sadd", "room", "corpse")

		local emoji
		local guild = get_guild()
		for e in guild.emojis do
			emoji = e
			break
		end

		reply("The pumpkin has been smashed. :skull:")
		reply(emoji.string..": \"B-but I'm still blocking the door. What will you do now?\"")
		return
	end
	reply("It looks like your attack did some damage :sweat_smile:")
	timer.sleep(500)
	reply(":jack_o_lantern:: **\"OW, I DON'T WANT THAT\"**")
	timer.sleep(1000)
	return reply(self:speech())
end
item_aliases["\240\159\142\131"] = "pumpkin" -- :jack_o_lantern:

items.corpse = {}
items.corpse.name = "The `corpse` of the hungry pumpkin."
items.corpse.article = "the rotting corpse"
items.corpse.plural_article = false
items.corpse.description = "It's probably delicious, but you don't feel so hungry."

items.adcbot = {}
items.adcbot.name = "Your humble `robot` narrator"
items.adcbot.article = "your humble narrator"
items.adcbot.plural_article = false
function items.adcbot:description()
	local guild = get_guild()
	for e in guild.emojis do
		emoji = e
		break
	end
	local state = redis('get', 'door_state')
	if state == 'unlocked' then
		return emoji.string..": \"It's me, your *heavy* narrator.\""
	end
	return emoji.string..": \"Don't mind me, I'm just your harmless narrator.\""
end

function items.adcbot:speech()
	local emoji
	local guild = get_guild()
	for e in guild.emojis do
		emoji = e
		break
	end
	local state = redis('get', 'door_state')
	if state == 'unlocked' then
		return emoji.string .. ": \"You won't be able to get through here, not while I'm in the way!\""
	end

	return emoji.string .. ": \"If you need help, you can always ask me!\""
end

item_aliases["robot"] = "adcbot"
item_aliases["gm"] = "adcbot"
item_aliases["narrator"] = "adcbot"
item_aliases["<@256986648036769833>"] = "adcbot"
item_aliases["<:beepboop:257361415256408064>"] = "adcbot"


items.room = {}
items.room.name = "The `room` you're in"
items.room.article = "the four walls around you"
items.room.hide = true
items.room.plural_article = true
function items.room:description()
	return function(reply)
		reply("You're in a spartan room, with a concrete floor and four slabs of drywall. Above you is only whiteness.")
		timer.sleep(1000)
		local msg = "Looking around, you notice:"
		local in_room = redis('smembers', 'room')
		for _, item_name in ipairs(in_room) do
			local item = items[item_name]
			if item.food == nil and item.book == nil and item.hide == nil then
				msg = msg .."\n *" .. tostring(item.name)
			end
		end
		msg = msg.."\nFeel free to look more closely by typing `!look <item>`."
		reply(msg)
	end
end
item_aliases["around"] = "room"
item_aliases["around town"] = "room"
item_aliases[""] = "room"

items.counter = {}
items.counter.name = "A small kitchen `counter`, off to the side."
items.counter.article = "the kitchen counter"
items.counter.plural_article = false
items.counter.description = function(self)
	local msg = "It's a straightforward counter, like from a kitchen or a diner. On it there is:"
	local in_room = redis('smembers', 'room')
	local nothing = true
	for _, item in ipairs(in_room) do
		if items[item].food ~= nil then
			nothing = false
			msg = msg .."\n *" .. tostring(items[item].name)
		end
	end
	if nothing == true then
		msg = msg .. "\n * Nothing!"
	end
	return msg
end
item_aliases["kitchen"] = "counter"
item_aliases["kitchen counter"] = "counter"
item_aliases["food zone"] = "counter"

items.salad = {}
items.salad.name = "A `salad`"
items.salad.food = true
items.salad.article = "the salad"
items.salad.plural_article = false
items.salad.punkin = "THE SALAD"
items.salad.blue = true
items.salad.description = function(self)
	return "You see a side salad. Attached is a receipt that says \"ORDER FOR: BLUE.\" It's missing the croutons..."
end
item_aliases["side salad"] = "salad"
item_aliases["greens"] = "salad"
item_aliases["the salad"] = "salad"

items.fries = {}
items.fries.name = "A plate of french `fries`"
items.fries.food = true
items.fries.article = "the plate of fries"
items.fries.plural_article = false
items.fries.punkin = "THE FRIES"
items.fries.red = true
items.fries.description = function(self)
	return "They're steak-cut french fries. Attached is a receipt that says \"ORDER FOR: RED.\" No ketchup, or mayo."
end
item_aliases["french fries"] = "fries"
item_aliases["plate of fries"] = "fries"
item_aliases["plate of french fries"] = "fries"
item_aliases["the plate of fries"] = "fries"
item_aliases["the plate of french fries"] = "fries"
item_aliases["the fries"] = "fries"

items.spaghetti = {}
items.spaghetti.name = "A bowl of `spaghetti`"
items.spaghetti.food = true
items.spaghetti.article = "the bowl of spaghetti"
items.spaghetti.plural_article = false
items.spaghetti.punkin = "THE PASTA"
items.spaghetti.yellow = true
items.spaghetti.description = function(self)
	return "It's a bowl of spaghetti, with meatballs. Attached is a receipt that says \"ORDER FOR: YELLOW.\" Be sure not to spill!"
end
item_aliases['pasta'] = "spaghetti"
item_aliases['the pasta'] = "spaghetti"
item_aliases["mom's spaghetti"] = "spaghetti"
item_aliases["moms spaghetti"] = "spaghetti"
item_aliases["bowl of spaghetti"] = "spaghetti"
item_aliases["the bowl of spaghetti"] = "spaghetti"

items.key = {}
items.key.name = "The door `key`!"
items.key.food = true
items.key.article = "the :key:"
items.key.plural_article = false
items.key.description = function(self)
	return "It's the :key:! It must have been under the plate of spaghetti."
end

item_aliases["the red book"] = "red_book"
item_aliases["red book"] = "red_book"
items.red_book = {}
items.red_book.name = "A `red book`."
items.red_book.book = true
items.red_book.article = "the red book"
items.red_book.plural_article = false
items.red_book.description = function(self)
	return "It's a red book. You can read it like so: `!read red book`."
end

items.red_book.text = [[
```
It was a dark and stormy night; the rain fell in torrents, except at occasional
intervals, when it was checked by a violent gust of wind which swept up the
streets (for it is in London that our scene lies), rattling along the
house-tops, and fiercely agitating the scanty flame of the lamps that struggled
against the --------. Through one of the obscurest quarters of London, and
among haunts little loved by the gentlemen of the police, a man, evidently of
the lowest orders, was wending his solitary way.
```
]]

local code_word = "cloudless darkness"

item_aliases["the blue book"] = "blue_book"
item_aliases["blue book"] = "blue_book"
items.blue_book = {}
items.blue_book.name = "A `blue book`."
items.blue_book.book = true
items.blue_book.article = "the blue book"
items.blue_book.plural_article = false
items.blue_book.description = function(self)
	return "It's a blue book. Are you sure you don't want to read something else?"
end
items.blue_book.text = [[
```
Should the ADC 1510 ever become unresponsive, or behave erratically, you can
disable it by speaking the code word printed on the sticker at the bottom of
this page. Enunciate clearly to make sure it is recognized by the voice
recognition software. The reboot and debugging procedures are on the following
pages.

DOING THIS WILL VOID YOUR WARRANTY!

CODE WORD --------- --------
```]]

item_aliases["the yellow book"] = "yellow_book"
item_aliases["yellow book"] = "yellow_book"
items.yellow_book = {}
items.yellow_book.name = "A `yellow book`."
items.yellow_book.book = true
items.yellow_book.article = "the yellow book"
items.yellow_book.plural_article = false
items.yellow_book.description = function(self)
	return "It's a book with a yellow cover. Read it using `!read`"
end
items.yellow_book.text = [[
```
It was a bitter night, so we drew on our ulsters and wrapped cravats about our
throats. Outside, the stars were shining coldly in a --------- sky, and the
breath of the passers-by blew out into smoke like so many pistol shots. Our
footfalls rang out crisply and loudly as we swung through the doctors’
quarter, Wimpole Street, Harley Street, and so through Wigmore Street into
Oxford Street. In a quarter of an hour we were in Bloomsbury at the Alpha Inn,
which is a small public-house at the corner of one of the streets which runs
down into Holborn. Holmes pushed open the door of the private bar and ordered
two glasses of beer from the ruddy-faced, white-aproned landlord.```]]

items.bookshelf = {}
items.bookshelf.name = "A `bookshelf`."
items.bookshelf.article = "the bookshelf"
items.bookshelf.plural_article = false
items.bookshelf.description = function(self)
	local msg = "A large, wooden bookshelf. There are a bunch of books but your video game sense only permits you to look at:"
	local in_room = redis('smembers', 'room')
	local nothing = true
	for _, item in ipairs(in_room) do
		if items[item].book ~= nil then
			nothing = false
			msg = msg .."\n *" .. tostring(items[item].name)
		end
	end
	if nothing == true then
		msg = msg .. "\n * Nothing!"
	end
	return msg
end

items.self = {}
items.self.name = "Your 'self', both in the abstract and the concrete sense"
items.self.article = "you"
items.self.plural_article = true
items.self.hide = true
items.self.description = function()
	return "Lookin' good. :100:"
end
item_aliases["myself"] = "self"
item_aliases["human flesh"] = "self"
item_aliases["the human flesh"] = "self"
-- }}}

synonyms["lookat"]  = "look"
synonyms["look at"] = "look"
synonyms["observe"] = "look"
synonyms["look in mirror"] = "look self"
function commands.look(reply, item_name)
	item_name = item_name or "room"
	if item_aliases[item_name] then
		item_name = item_aliases[item_name]
	end

	if redis('get', 'angry_pumpkin') == 'true' and item_name ~= "pumpkin" then
		reply("There's no time, you have to deal with the :jack_o_lantern:!")
		return
	end


	local item = items[item_name]
	local ret = redis('sismember', 'room', item_name)
	if not item or ret == 0 then
		reply("I don't see an item like that. you can type `!look room` to get a closer look around the room.", 'private')
		return
	end
	local desc = item.description
	if type(desc) == 'string' then
		reply(desc)
	else
		local resp = desc(item)
		if type(resp) == 'string' then
			reply(resp)
		else
			resp(reply)
		end
	end
end

function commands.read(reply, item_name)
	item_name = item_name or "room"
	if item_aliases[item_name] then
		item_name = item_aliases[item_name]
	end

	if redis('get', 'angry_pumpkin') == 'true' and item_name ~= "pumpkin" then
		reply("There's no time, you have to deal with the :jack_o_lantern:!")
		return
	end

	if item_name == "book" then
		reply("You're going to have to be more specific than that, sorry.")
		return
	end


	local item = items[item_name]
	local ret = redis('sismember', 'room', item_name)
	if not item or ret == 0 then
		reply("I don't see an item like that. you can type `!look room` to get a closer look around the room.", 'private')
		return
	end
	if not item.text then
		reply("I can't read it. There's-")
		timer.sleep(500)
		reply("there's no words there.")
	end

	reply("It says:\n"..item.text)
	if item_name == "blue_book" then
		timer.sleep(1000)
		local emoji
		local guild = get_guild()
		for e in guild.emojis do
			emoji = e
			break
		end
		reply(emoji.string..[[: "No, I'm not going to read that to you."]])
	end
end

synonyms["speak with"] = "talk"
synonyms["speak"]      = "talk"
synonyms["talk to"]    = "talk"
synonyms["listen"]     = "talk"
synonyms["listen to"]  = "talk"
function commands.talk(reply, item_name)
	item_name = item_name or "room"
	if item_aliases[item_name] then
		item_name = item_aliases[item_name]
	end

	if redis('get', 'angry_pumpkin') == 'true' and item_name ~= "pumpkin" then
		reply("There's no time, you have to deal with the :jack_o_lantern:!")
		return
	end

	local item = items[item_name]
	local ret = redis('sismember', 'room', item_name)
	if not item or ret == 0 then
		reply("I don't see an item like that. you can type `!look room` to get a closer look around the room.", 'private')
		return
	end

	if not item.speech then
		local msg = [[You give an impassioned speech, but %s %s having any of it.]]
		msg = string.format(msg, item.article, item.plural_article and "aren't" or "isn't")
		reply(msg)
		return
	end
	local desc = item.speech
	if type(desc) == 'string' then
		reply(desc)
	else
		local resp = desc(item)
		if type(resp) == 'string' then
			reply(resp)
		else
			resp(reply)
		end
	end
end

synonyms["smash"]   = "attack"
synonyms["destroy"] = "attack"
function commands.attack(reply, item_name, author)
	item_name = item_name or "room"
	if item_aliases[item_name] then
		item_name = item_aliases[item_name]
	end

	if redis('get', 'door_state') ~= 'unlocked' then
		reply("This is not a violent place, my friend. Chill~")
		return
	end

	if item_name == 'pumpkin' then
		return items.pumpkin:attack(reply, author)
	else
		local item = items[item_name]
		local ret = redis('sismember', 'room', item_name)
		if not item or ret == 0 then
			reply("I don't see an item like that. you can type `!look room` to get a closer look around the room.", 'private')
			return
		end
		local msg = ("You swing wildly at %s, but you only manage to hurt yourself.")
		msg = msg:format(item and item.article or "the thing")
		reply(msg)
	end
end

function commands.give(reply, item_name, recipient_name, user)
	if item_aliases[item_name] then
		item_name = item_aliases[item_name]
	end
	if item_aliases[recipient_name] then
		recipient_name = item_aliases[recipient_name]
	end

	local item = items[item_name]
	local recipient = items[recipient_name]
	local ret = redis('sismember', 'room', item_name)
	if not item or ret == 0 then
		reply("I don't see an item like that. you can type `!look room` to get a closer look around the room.", 'private')
		return
	end
	if item.red and not is_red(user) then
		reply("This isn't yours to give away! Maybe somebody else can, though...")
		return
	elseif item.blue and not is_blue(user) then
		reply("This isn't yours to give away! Maybe somebody else can, though...")
		return
	elseif item.yellow and not is_yellow(user) then
		reply("This isn't yours to give away! Maybe somebody else can, though...")
		return
	end

	ret = redis('sismember', 'room', recipient_name)
	if not recipient or ret == 0 then
		reply("Who are you trying to give this to, exactly?", 'private')
		return
	end

	if not recipient.recieve then
		local msg = [[%s %s interested, unfortunately.]]
		msg = string.format(msg, recipient.article, recipient.plural_article and "aren't" or "isn't")
		reply(msg)
		return
	end
	local response = recipient:recieve(item_name, item)
	if not response then
		local msg = [[%s %s interested, unfortunately.]]
		msg = string.format(msg, recipient.article, recipient.plural_article and "aren't" or "isn't")
		reply(msg)
		return
	elseif type(response) == 'function' then
		response(reply, user)
	else
		reply(response)
	end
end

synonyms["unlock"] = "use key on"
function commands.use(reply, a_name, b_name)
	if item_aliases[a] then
		a_name = item_aliases[a_name]
	end
	if item_aliases[b_name] then
		b_name = item_aliases[b_name]
	end

	local a = items[a_name]
	local b = items[b_name]
	local ret = redis('sismember', 'room', a_name)
	if not a or ret == 0 then
		if a_name == 'key' then
			reply("I don't see a key to unlock it with.", 'private')
			return
		end
		reply("I don't see an item like that. you can type `!look room` to get a closer look around the room.", 'private')
		return
	end

	local ret = redis('sismember', 'room', b_name)
	if not b or ret == 0 then
		reply("Who are you trying to give this to, exactly?", 'private')
		return
	end

	if not b.use then
		local msg = [[%s %s interested, unfortunately.]]
		msg = string.format(msg, a.article, a.plural_article and "aren't" or "isn't")
		reply(msg)
		return
	end
	local response = b:use(a_name, a)
	if not response then
		local msg = [[%s %s interested, unfortunately.]]
		msg = string.format(msg, a.article, a.plural_article and "aren't" or "isn't")
		reply(msg)
		return
	elseif type(response) == 'function' then
		response(reply)
	else
		reply(response)
	end
end

function commands.no_use(reply, a_name, b_name)
	if a_name and not b_name then
		reply("What do you want to use this on?")
	else
		reply("I don't get it. Try using this wording: `!use X on Y`")
	end
end

function commands.open(reply, item_name)
	if item_aliases[item_name] then
		item_name = item_aliases[item_name]
	end
	if item_name:match("book") then
		return commands.read(reply, item_name)
	elseif item_name == "door" then
		reply("It's locked. type `!look door` to get a closer look.")
	else
		reply("I don't think you can open that.")
	end
end

function commands.move(reply, item_name)
	item_name = item_name or "room"
	if item_aliases[item_name] then
		item_name = item_aliases[item_name]
	end

	if redis('get', 'angry_pumpkin') == 'true' and item_name ~= "pumpkin" then
		reply("There's no time, you have to deal with the :jack_o_lantern:!")
		return
	end

	local item = items[item_name]
	local ret = redis('sismember', 'room', item_name)
	if not item or ret == 0 then
		reply("I don't see an item like that. you can type `!look room` to get a closer look around the room.", 'private')
		return
	end

	if item.food then
		reply("I bet you're real fun at parties. :expressionless:")
	end

	reply("You shove and you shove, but it just won't budge.")
end
synonyms["shove"] = "move"
synonyms["push"] = "move"

local intro = {
"Hey buddo, welcome to my room! I know we'll have a great ti-",
"...What, you wanted to get over there, to the Cool People Room?",
"Not so fast there, the entrance is locked! You can type `!look door` to get a closer look."
}
function commands.intro(reply)
	reply(intro[1])
	timer.sleep(2000)
	reply(intro[2])
	timer.sleep(1000)
	reply(intro[3])
end

local iunno = "What? I'm not sure I understood that. you can type `!help` if you're lost, too."
function commands.iunno(reply)
	reply(iunno, 'private')
end

local help = [[
I'm not the smartest robot in the world, but I know a little bit of english, at
least. Normally, I'll tell you copy-pastable things like so: `!look door`, but
otherwise, here are the main things I should be able to understand:
```
!look (at) X
!talk (to) X
!give X to Y
!use X on Y
!attack X
!unlock X
!read X
!help
!intro
!inventory
```
]]

synonyms["halp"] = "help"
synonyms["pls"]  = "help"
function commands.help(reply)
	reply(help, 'private')
end

function commands._what(reply)
	reply("What do you just say?")
end

synonyms["consume"] = "eat"
synonyms["devour"] = "eat"
function commands.eat(reply)
	reply("You're not hungry. :rolling_eyes:")
end

