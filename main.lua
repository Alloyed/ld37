local dsc = require 'discordia'
_G.redis_client = require 'redis-client'
local timer = require 'timer'

local client = dsc.Client()

_G.timer = timer
_G.client = client

require('bot')(client)

client:run(os.getenv('ADCBOT_TOKEN'))
